package se325.lab02.concert.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public class ConcertApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ConcertApplication() {
        singletons.add(new ConcertResource());
        _classes.add(SerializationMessageBodyReaderAndWriter.class);
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
    
    private Set<Class<?>> _classes = new HashSet<Class<?>>();

    @Override
    public Set<Class<?>> getClasses() {
      return _classes;
    }
}
